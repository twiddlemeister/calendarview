//
//  CalendarCollectionViewCell.swift
//  CalendarView
//
//  Created by David Mountain on 05/08/2019.
//  Copyright © 2019 David Mountain. All rights reserved.
//

import UIKit
import SnapKit

enum Category: Int {
    static var random: Category {
        return Category(rawValue: Int.random(in: 0...3))!
    }
    
    case none
    case today
    case compliant
    case nonCompliant
    
    var color: UIColor {
        if self == .today {
            return .red
        } else if self == .compliant {
            return .blue
        } else if self == .nonCompliant {
            return .orange
        } else {
            return .clear
        }
    }
}

struct Day {
    let position: CGPoint
    let isToday: Bool
    let category: Category
}

class CalendarMonthView: UIView {
    var days = [Day]() {
        didSet {
            setNeedsDisplay()
        }
    }
    
    init() {
        super.init(frame: .zero)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        isOpaque = false
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    let rows = 6
    let cols = 7
    lazy var cellSize = bounds.width / CGFloat(cols)
    
    override func draw(_ rect: CGRect) {
        // Draw blobs around consecutive groups of days with the same category
        days.reduce(into: [(category: Category, days: [Day])]()) {
            if $1.category != $0.last?.days.last?.category {
                $0 += [($1.category, [$1])]
            } else {
                $0[$0.count - 1].days += [$1]
            }
        } .forEach { group in
            group.days.enumerated().reduce(into: [[Day]]()) {
                if $1.offset == 0 || $1.offset > 0 && $1.element.position.y > group.days[$1.offset - 1].position.y {
                    $0 += [[$1.element]]
                } else {
                    $0[$0.count - 1] += [$1.element]
                }
            } .forEach { row in
                let rect = CGRect(x: (row.first?.position.x ?? 0) * cellSize,
                                  y: (row.first?.position.y ?? 0) * cellSize,
                                  width: CGFloat(row.count) * cellSize,
                                  height: cellSize).applying(.init(translationX: 0, y: -2))
                
                let bezier = UIBezierPath(roundedRect: rect, cornerRadius: cellSize / 2)
                group.category.color.setFill()
                bezier.fill()
            }
        }
        
        // Draw day number
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        
        for (index, day) in days.enumerated() {
            let rect = CGRect(origin: .init(x: day.position.x * cellSize, y: day.position.y * cellSize),
                              size: .init(width: cellSize, height: cellSize))
            
            ("\(index + 1)" as NSString).draw(in: rect, withAttributes: [
                .foregroundColor: day.category == .none ? UIColor.black : UIColor.black,
                .paragraphStyle: paragraph
            ])
        }
    }
}

class CalendarCollectionViewCell: UICollectionViewCell {
    override class var requiresConstraintBasedLayout: Bool { return true }
    
    var days = [Day]() {
        didSet {
            monthView.days = days
        }
    }
    
    let monthLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.text = "Month"
        return label
    }()
    
    let monthView = CalendarMonthView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        addMonthLabel()
        addMonthView()
    }
    
    private func addMonthLabel() {
        contentView.addSubview(monthLabel)
        
        monthLabel.snp.remakeConstraints { make in
            make.leading.top.trailing.equalToSuperview()
        }
    }
    
    private func addMonthView() {
        contentView.addSubview(monthView)
        
        monthView.snp.remakeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.top.equalTo(monthLabel.snp.bottom).offset(8)
        }
    }
}
