//
//  CalendarCollectionViewLayout.swift
//  CalendarView
//
//  Created by David Mountain on 05/08/2019.
//  Copyright © 2019 David Mountain. All rights reserved.
//

import UIKit

class CalendarCollectionViewLayout: UICollectionViewFlowLayout {
    override init() {
        super.init()
        minimumLineSpacing = 12
        minimumInteritemSpacing = 4
        sectionInset = .init(top: 4, left: 4, bottom: 4, right: 4)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepare() {
        super.prepare()
        
        let sideLength = (collectionView!.bounds.width / 3)
        let size = CGSize(width: sideLength - 8, height: sideLength)
        
        estimatedItemSize = size
        itemSize = size
    }
}
