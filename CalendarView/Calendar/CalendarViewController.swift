//
//  CalendarViewController.swift
//  CalendarView
//
//  Created by David Mountain on 05/08/2019.
//  Copyright © 2019 David Mountain. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class CalendarViewController: UICollectionViewController {
    var data = [[Day]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView!.register(CalendarCollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        collectionView.collectionViewLayout = CalendarCollectionViewLayout()
        
        data = months(for: 2019)
    }
    
    var todayComponents: DateComponents {
        return Calendar.current.dateComponents([.year, .month, .day], from: Date())
    }
    
    private func months(for year: Int) -> [[Day]] {
        return (1...12).map { days(forMonth: $0, in: year) }
    }
    
    private func days(forMonth month: Int, in year: Int) -> [Day] {
        let calendar = Calendar.current
        
        var components = DateComponents()
        components.year = year
        components.month = month
        components.day = 1
        
        guard let date = calendar.date(from: components),
              let dayCount = calendar.range(of: .day, in: .month, for: date)?.count else
        {
            return []
        }
        
        let weekday = calendar.component(.weekday, from: date)
        
        // A weekday component can have a value of 1 to 7 for the Gregorian calendar,
        // where 1 is Sunday. We need to take off one from the weekday to make the start
        // of the week Monday (wrapping around properly for Sunday), and then take another
        // one off to ensure the offset is zero for Monday for the purposes of laying out
        // the days in a grid.
        let offset = (weekday == 1 ? 7 : weekday - 1) - 1
        
        var fudgery = 0
        var currentCategory = Category.none
        
        return (offset..<dayCount + offset).enumerated().map {
            let (row, col) = $0.element.quotientAndRemainder(dividingBy: 7)
            let isToday = $0.offset + 1 == todayComponents.day && month == todayComponents.month && year == todayComponents.year
            return .init(position: .init(x: col, y: row), isToday: isToday, category: {
                if fudgery == Int.random(in: 2...4) {
                    fudgery = 0
                    currentCategory = Category.random
                } else {
                    fudgery += 1
                }
                
                return currentCategory
            }())
        }
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! CalendarCollectionViewCell
        cell.days = data[indexPath.row]
        cell.monthLabel.text = Calendar.current.monthSymbols[indexPath.row]
        return cell
    }

    // MARK: UICollectionViewDelegate
    
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
}
